$(document).ready(function() {
    let conf = {
        'api_url': '../public/index.php/api/',
        'endpoints': {
            'weather': 'weather'
        },
        'endpoint': function(endpoint_key) {
            return this.api_url + this.endpoints[endpoint_key];
        },
        'openweather_token': '693c347207d25ade8155baabaca8fd30',
        'openweather_api': 'http://api.openweathermap.org/data/2.5/weather',
        'openweather_data': function(queryArray) {
            return {
                'q': queryArray.join(','),
                'appid': this.openweather_token,
                'units': 'metric'
            }
        },
        'weatherapi_token': 'c3b9bad0102b4f63ab9224230200711',
        'weatherapi_api': 'http://api.weatherapi.com/v1/current.json',
        'weatherapi_data': function(queryArray) {
            return {
                'key': this.weatherapi_token,
                'q': queryArray.join(',')
            }
        },
    };

    let t = null;

    function changeSelectedLocation(val) {
        let city = $('#city');
        city.find('option').hide();
        city.find('option[data-country="' + val + '"]').show();
        city.val(null);
        $('#curent-weather-box').addClass('hide');
    }

    function dateFormat(outputDate) {
        let d = new Date(outputDate);

        return [d.getMonth() + 1,
            d.getDate(),
            d.getFullYear()
        ].join('-') + ' ' + [d.getHours(),
            d.getMinutes(),
            d.getSeconds()
        ].join(':');
    }

    function performData(openweather, weatherapi) {
        let output = [];

        if (!!openweather) { //perform openweather
            output.push(openweather[0].main.temp);
        }

        if (!!weatherapi) { //perform weatherapi
            output.push(weatherapi[0].current.temp_c);
        }

        var sum = output.reduce(function(a, b) {
            return a + b;
        });

        return {
            'avg': parseInt((sum / output.length) * 100) / 100
        }
    }

    function initWeatherTable() {
        $.get(conf.endpoint('weather'), function(data) {
            let tableWeather = $('#table-weather');
            tableWeather.removeClass('loading');
            t = tableWeather.DataTable({
                // 'ajax': conf.endpoint('weather'),
                'dataSrc': '',
                'data': data,
                'columns': [{
                        'data': 'country'
                    },
                    {
                        'data': 'city'
                    },
                    {
                        'data': 'createDate'
                    },
                    {
                        'data': 'weatherDeg'
                    }
                ],
                'columnDefs': [{
                    targets: 2,
                    render: function(data) {
                        return dateFormat(data);
                    }
                }]
            });
        }, 'json');
    }

    changeSelectedLocation($('#country option:selected').data().country);

    initWeatherTable();

    $('#country').on('change', function(e) {
        changeSelectedLocation($(e.currentTarget).find('option:selected').data().country);
    });

    $('#city').on('change', function(e) {

        $.when(
            $.get(conf.openweather_api, conf.openweather_data([
                e.currentTarget.value,
                $('#country').val()
            ]), 'json'),
            $.get(conf.weatherapi_api, conf.weatherapi_data([
                e.currentTarget.value,
                $('#country').val()
            ]), 'json')
        ).then(function(openweather, weatherapi) {
            let data = performData(openweather, weatherapi);
            let addRow = {
                "weatherDeg": data.avg.toString(),
                "country": $('#country').find('option[value="' + $('#country').val() + '"]').text(),
                "city": $('#city').find('option[value="' + $('#city').val() + '"]').text(),
                "createDate": new Date().toISOString() //"2020-11-08T00:53:43.697Z"
            };


            $.ajax({
                url: conf.endpoint('weather'),
                type: "POST",
                data: JSON.stringify(addRow),
                accept: "application/ld+json",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function() {
                    // initWeatherTable(); //@TODO

                    t.row.add(
                        {
                            city: addRow["city"],
                            country: addRow["country"],
                            createDate: dateFormat(addRow["createDate"]),
                            weatherDeg: addRow["weatherDeg"]
                        }
                    ).draw(false);
                }
            });

            let deg = data.avg.toString().split('.');

            $('.mainDeg').text(deg[0]);
            $('.decimalDeg').text(deg[1]);

            $('#curent-weather-box').removeClass('hide');
        });
    });
});